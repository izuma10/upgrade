create table equipos(
    equipo VARCHAR (30),
    comunidad_autonoma  VARCHAR(30),
    provincia VARCHAR(30),
    ciudad VARCHAR(30),
    estadio VARCHAR(30),
    capacidad_estadio int,
    primary key (equipo)
    );