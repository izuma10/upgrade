import React, {Component} from 'react';



class Clock extends Component {
    constructor(props){
        super(props);
        this.state={
            date:new Date()
        };
    }

    componentDidMount(){
        this.intervalID = setInterval(
            ()=>this.tick(),
            1000
        );
    }

    componentWillUnmount(){
        clearInterval(this.intervalID);
    }

    tick(){
        this.setState({
            date : new Date()
        });
    }

    render() {
        return ( 
            <div>
                <p className = "App-clock" >
                La hora es <label>{this.state.date.toLocaleTimeString()}</label>
                </p>
            </div>
        );
    }
}

export default Clock;