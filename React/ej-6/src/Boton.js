import React, {Component} from 'react';
import MasMenos from './MasMenos';


class Boton extends Component{
    constructor(props){
        super(props);
        this.toca =this.toca.bind(this);
        this.state= {items:[]}
    }

    toca(e){
        let newArray = this.state.items;
        let l= newArray.length;
        newArray.push(<MasMenos key={l}></MasMenos>);
        this.setState({
        items: newArray
    });
    }
    render(){
        return(
            <div>
                <div>
                    <button onClick= {this.toca}>Otro Contador</button>
                </div>
                <div>
                {this.state.items}
                </div>
            </div>
        );
    }

}


export default Boton;