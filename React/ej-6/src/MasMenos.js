import React, {Component} from 'react';

class MasMenos extends Component{
    constructor(){
        super();
        this.pulsarBotonMas = this.pulsarBotonMas.bind(this);
        this.pulsarBotonMenos = this.pulsarBotonMenos.bind(this);
        this.state = {count: 0}
    }
    pulsarBotonMas(e) {
        this.setState({
            count: this.state.count + 1,
            clicked: true
        });
    }
    pulsarBotonMenos(e){
        this.setState({ 
            count:this.state.count -1,
            clicked: true
        });
    }
    render() {
        return ( 
            <div>
                <button onClick = { this.pulsarBotonMas } >Pulsar para sumar</button>
                <button onClick = { this.pulsarBotonMenos } >Pulsar para restar </button>
                <p>
                    Contador: { this.state.count }
                </p>
            </div>

        );
    }
}


export  default  MasMenos;