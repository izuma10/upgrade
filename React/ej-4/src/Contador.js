import React, { Component } from 'react';

class Contador extends Component {
    constructor() {
        super();
        this.pulsarBoton = this.pulsarBoton.bind(this);
        this.state = {count: 0}
    }
    pulsarBoton(e) {
        this.setState({ count: this.state.count + 1 });
        this.setState({ clicked: true });
    }
    render() {
        return ( 
            <button onClick = { this.pulsarBoton } > Contador: { this.state.count } </button>
        );
    }
}

export default Contador;